Node-Express-Postgres-Sequelize

# node initialization
npm init

## Express for api
npm install express --save

## for global environment variables 
npm install dotenv

## Object Relation Model for Postgres
npm install --save sequelize

npm install --save pg pg-hstore

## For Logging issues
npm install morgan

