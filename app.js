const express = require('express');
const morgan = require('morgan');
const dotenv = require('dotenv').config({ path: 'config/.env' });
const db = require('./config/db');
const app = express();

app.use(express.json());

//logging
app.use(morgan('tiny'));

//routes
const user = require('./routes/user');

//content-type
app.use(function(req, res, next) {
    res.setHeader("Content-Type", "application/json");
    next();
});


//main route
app.get('/', (req, res) => {
  res.status(200).send({
      message: "selam",
      from: "server"
  });  
});

//routes
app.use('/user', user);

//startup
app.listen(process.env.PORT, () => {
    console.log(`Server is running on ${process.env.PORT}`);

    //database test connection
    db.authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

});

