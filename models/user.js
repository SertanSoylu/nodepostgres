const Sequelize = require('sequelize');
const db = require('../config/db');

const User = db.define('user', {
    id : {
        type: Sequelize.INTEGER,
        autoIncrement : true,
        primaryKey: true,
        allowNull: false
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    surname: {
      type: Sequelize.STRING,
      allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    bdate: {
        type: Sequelize.DATE,
        allowNull: true   
    },    
    sex_id: {
        type: Sequelize.INTEGER,
        allowNull: false   
    },    
}, {
    // options
    timestamps: false,
    freezeTableName: true,
});

module.exports = User;


