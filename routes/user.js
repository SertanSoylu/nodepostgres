const express = require('express');
const router = express.Router();

const db = require('../config/db');
const User = require('../models/user');

//Get All Users
router.get('/', (req,res) => {
    User.
    findAll()
    .then((users) => {
        res.send(users);
    })
    .catch((err)=> {
        res.send({
            error: true,
            message: err
        })
    });
    
    
});

//Get User By ID
router.get('/:id', (req,res) => {
    User.
    findAll({
        where:  {
            id: req.params.id,
        },
    })
    .then((users) => {
        res.send(users);
    })
    .catch((err)=> {
        res.send({
            error: true,
            message: err
        })
    });
    
});

//Insert a User
router.post('/', (req,res) => {
    //console.log(req.body);
    let {name, surname, email, bdate, sex_id} = req.body;

    User.
    create({
        name,
        surname,
        email,
        bdate,
        sex_id
    })
    .then((user) => {
        res.send(user);
    })
    .catch((err)=> {
        res.send({
            error: true,
            message: err
        })
    });
    
});

//update a User
router.put('/:id', (req,res) => {
    let {email} = req.body;

    User.
    update({
        email
    }, {
        where: {
            id: req.params.id
        }
    })
    .then((user) => {
        res.send(user);
    })
    .catch((err)=> {
        res.send({
            error: true,
            message: err
        })
    });
    
});

//delete a User
router.delete('/:id', (req,res) => {
    User.
    destroy({
        where: {
            id: req.params.id
        }
    })
    .then((user) => {
        console.log(user);
        res.statusCode(200);
        res.send({
            error: false,
            message: "user deleted"
        });
    })
    .catch((err)=> {
        res.send({
            error: true,
            message: err
        })
    });
    
});


module.exports = router;